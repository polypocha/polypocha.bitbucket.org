import players from './players.js';

function reduxTest(initialState,action,expected){
  var actual= players(initialState, action);
  expect(actual).toEqual(expected);
  return actual;
}

test('adds 1 + 2 to equal 3', () => {
  expect(1 + 2).toBe(3);
});

test('no action changes nothing', () => {
  var expected = {hola:true}
  var actual = players(expected,{type:'unknown', color:'red'});
  expect(actual).toBe(expected);
});

test('first user shoudl be created', () => {
  var action={type:'setupPlayerAdd', player:20, color:'red'}
  var initialState=[]
  var expected=[{order:20, id:20, color:'red', portrait:null}]
  reduxTest(initialState,action,expected);
});

test('second user should be created also', () => {
  var action={type:'setupPlayerAdd', player:22, color:'blue'}
  var initialState=[{order:20, id:20, color:'red', portrait:null}]
  var expected=[{order:20, id:20, color:'red', portrait:null},{order:22, id:22, color:'blue', portrait:null}]
  reduxTest(initialState,action,expected);
})

test('a player can add a photo', () => {
  var action = {type: 'setupPlayerSetPortrait', player:22, src:'./favicon.ico'}
  var initialState= [{order:21, id:20, color:'red', portrait:null},{order:23, id:22, color:'blue', portrait:null}]
  var expected=[{order:21, id:20, color:'red', portrait:null},{order:23, id:22, color:'blue', portrait:'./favicon.ico'}]
  reduxTest(initialState,action,expected);
})

test('a a non-player can leave the game without harm', () => {
  var action = {type: 'setupPlayerRemove', player:26}
  var initialState = [{order:26, id:20, color:'red', portrait:null},{order:22, color:'blue', portrait:'./favicon.ico'}]
  var expected = initialState
  reduxTest(initialState,action,expected);
})

test('a player can leave the game', () => {
  var action = {type: 'setupPlayerRemove', player:22}
  var initialState= [{order:20, id:20, color:'red', portrait:null},{order:21, id:22, color:'blue', portrait:'./favicon.ico'}]
  var expected=[{order:20, id:20, color:'red', portrait:null}]
  reduxTest(initialState,action,expected);
})
