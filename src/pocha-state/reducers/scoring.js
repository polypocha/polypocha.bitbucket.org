const scoring = (state = {round: 0, score: {}}, action) => {
  switch (action.type) {
    case 'SETUP_PLAYER_REMOVE': {
      let score = Object.keys(state.score).reduce((obj, key) => {
        if(key != (action.player)) { // types are different!
          let item = state.score[key];
          return Object.assign({}, obj, {[key]: item});
        } else {
          return obj;
        }
      }, {});
      return Object.assign({}, state, {score} );
    }
    case 'SCORE_CHANGE': {  // player:1, tips:2, 
      let playerScore = state.score[action.player];
      let newPlayerScore = {
        current: playerScore?playerScore.current:0,
        tips: action.tips,
      };
      let score = Object.assign(
        {},
        state.score,
        {[action.player]: newPlayerScore});
      return Object.assign({}, state, {score} );
    }
    case 'ADD_ROUND': {
      let round = state.round +1;
      let score = Object.keys(state.score).reduce((acc, key) => {
        let current = action.score[key];
        return Object.assign({}, acc, {[key]: {
          current: current,
          tips: null,
        }});
      }, {});
      return {round, score};
    }
    default:
      return state;
  }
};
