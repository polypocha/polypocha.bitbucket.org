const preGlobal = (state = [], action) => {
  switch (action.type) {
    case 'RESET':
      return {};
    default:
      return state;
  }
};
