const reduxActions = function (){
  o = {};
  
  let pochaRulesElement = document.createElement("pocha-rules");

  o.addPlayer = () => {
    return {type: 'SETUP_PLAYER_ADD'};
  };
  o.removePlayer = (id) => {
    return {type: 'SETUP_PLAYER_REMOVE', player: id};
  };
  o.setPortrait = (id, src) => {
    return {type: 'SETUP_PLAYER_SET_PORTRAIT', player: id, src: src};
  };
  o.changeRules = (rule) => {
    return {type: 'RULE_SET', rule};
  };
  o.changeRuleRoundFlags = (flags) => {
    return {type: 'RULE_ROUND_FLAGS', flags};
  };
  o.setScore = (id, tips) => {
    return {type: 'SCORE_CHANGE', player: id, tips};
  };
  o.newRound = (score) => {
    return {type: 'ADD_ROUND', score};
  };
  o.reset = () => {
    return {type: 'RESET'};
  };
  return o;
}();

